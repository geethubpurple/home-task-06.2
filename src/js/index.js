require.config({
    baseUrl: './js',
    paths: {
        jquery: './libs/jquery'
    }
});

require(['Bike', 'jquery'], function (Bike, $) {
    $(function () {
        $.get('./data/bikes.json', function (data) {
            data.bikes.forEach(function (bike) {
                new Bike(bike, $('<div>').addClass('bike-wrap').appendTo(document.body));
            });
        });
    })
});